# Banner Templates

Olá!

Então isto é onde guardamos as templates do [banner maker](https://banners.vatsim.pt/) da equipa de eventos.

# Como funciona?
> Com muito cuidado

O Banner Maker vai a este repositório público buscar o ficheiro [config.json](https://gitlab.com/portugal-vacc/banners/banner-templates/-/blob/master/config.json?ref_type=heads). 

![welcome](./example/welcome.png){width=40%}

A lista de templates corresponde ao nome do parametro name de cada item do ``config.json``

![templatelist](./example/templatelist.png){width=40%}


## ``config.json`` descreve como vai ser feito o banner

### Parâmetros globais:
- ``name`` 
- ``type``                      ("cpt" ou "event")
- ``text_right``                ("true" ou "false")
- ``background_image_url``      (url absolute link)
- ``logo_1_url``                (url absolute link)
- ``logo_2_url``                (url absolute link)
- ``logo_3_url``                (url absolute link)

### Parâmetros específicos ao type CPT:
- ``call_sign``
- ``rating``

### Parâmetros de formulário:
- Para evento:
    - Subtitle
- Para CPT:
    - Controller Name

# Já fiz o que tinha a fazer. E agora?

Basta fazer refresh no banner maker no browser que já irá aplicar as novas atualizações :)

> Xpetáculoooo


# Exemplos

## Exemplo Evento Base

- Três posições dos ``logo_1``, ``logo_2`` e ``logo_3``
- Parâmetro ``name``
- Parâmetro do formulário "Subtitle"
- Parâmetro do formulário "Date"

### ``config.json``
```
{
    "name": "demonstração_event_text_right_false",
    "type": "event",
    "text_right": false,
    "background_image_url": "https://gitlab.com/portugal-vacc/banner-templates/-/raw/master/example/example.jpg",
    "logo_1_url": "https://gitlab.com/portugal-vacc/banner-templates/-/raw/master/logos/LogoPvACC.png",
    "logo_2_url": "https://gitlab.com/portugal-vacc/banner-templates/-/raw/master/logos/LogoPvACC.png",
    "logo_3_url": "https://gitlab.com/portugal-vacc/banner-templates/-/raw/master/logos/LogoPvACC.png"
}
```

### Formulário
![formcpt](./example/formevent.png){width=40%}

### Resultado
![1](./example/demonstração_event_text_right_false.jpg){width=40%}

## Parâmetro Text Right

### ``config.json``
```
{
    "name": "demonstração_event_text_right_true",
    "type": "event",
    "text_right": true,
    "background_image_url": "https://gitlab.com/portugal-vacc/banner-templates/-/raw/master/example/example.jpg",
    "logo_3_url": "https://gitlab.com/portugal-vacc/banner-templates/-/raw/master/logos/LogoPvACC.png"
}
```

### Resultado
![2](./example/demonstração_event_text_right_true.jpg){width=40%}


## Exemplo CPT

- Rating
- Call Sign
- Parâmetro do formulário "Controller Name"

### Formulário
![formcpt](./example/formcpt.png){width=40%}

### ``config.json``
```
{
    "name": "Demonstração CPT",
    "type": "cpt",
    "call_sign": "VERY_NICE",
    "rating": "C4",
    "text_right": false,
    "background_image_url": "https://gitlab.com/portugal-vacc/banner-templates/-/raw/master/example/example.jpg",
    "logo_3_url": "https://gitlab.com/portugal-vacc/banner-templates/-/raw/master/logos/LogoPvACC.png"
}
```

### Resultado
![3](./example/controller_name_goes_here.jpg){width=40%}


# Questões, dúvidas ou inquietações
> Chama o Simões






